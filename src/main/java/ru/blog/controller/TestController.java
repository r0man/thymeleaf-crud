package ru.blog.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.blog.entity.Post;
import ru.blog.entity.university.Student;
import ru.blog.entity.university.StudentId;
import ru.blog.exception.NotFoundItem;
import ru.blog.repository.*;
import ru.blog.service.CategoryService;
import ru.blog.service.ContactService;
import ru.blog.service.PostService;

import java.util.List;

@Controller
@AllArgsConstructor
public class TestController {

    private final PostService postService;
    private final ContactService contactService;
    private final CategoryRepository categoryRepository;
    private final StudentRepository studentRepository;
    private final StudentIdRepository studentIdRepository;

    @GetMapping("/test")
    @ResponseBody
    public String getMethod() {
        System.out.println("----------------");
        Post post = postService.getPostById(1L).orElseThrow(NotFoundItem::new);
        System.out.println("----------------");
        System.out.println("post title: " + post.getTitle() + " post category: " + post.getCategory().getName());

        return "";
    }

    @GetMapping("/test2")
    @ResponseBody
    public String getMethod2() {
        System.out.println("----------------");
        List<CategoryIdAndName> categories = categoryRepository.getCategoriesByActive(true);
        System.out.println("----------------");

        return "";
    }

    @GetMapping("/test3")
    @ResponseBody
    public String getMethod3() {
        System.out.println("----------------");
        Student student = studentRepository.findById(1L).orElseThrow(RuntimeException::new);
        System.out.println("student name: " + student.getName() +
                " student id number:" + student.getStudentId().getNumber());
        System.out.println("----------------");

        StudentId studentId = studentIdRepository.findById(1L).orElseThrow(RuntimeException::new);
        System.out.println("student name: " + studentId.getStudent().getName() + " student id number:" + studentId.getNumber());
        return "";
    }

    @GetMapping("/test4")
    @ResponseBody
    public String getMethod4() {
        System.out.println("----------------");
        Post post = postService.getPostById(1L).orElseThrow(RuntimeException::new);
        System.out.println("title: " + post.getTitle() + " author: ");

        System.out.println("----------------after main select");
        post.getUser().getName();


        return "";
    }
}
