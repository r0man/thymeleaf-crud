package ru.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class HomeController {

    @GetMapping("/login")
    public String login(@RequestParam(required = false) Optional<String> error, Model model) {
        System.out.println("hello");
        if (error.isPresent()) {
            model.addAttribute("error", "Invalid Credentials provided");
        }

        return "login";
    }
}
