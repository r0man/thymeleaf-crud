package ru.blog.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.blog.entity.User;
import ru.blog.repository.UserRepository;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        System.out.println("-----auth");
        ru.blog.entity.User userDetails = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Not found user"));

        return new org.springframework.security.core.userdetails.User(
                userDetails.getName(),
                userDetails.getPassword(),
                userDetails.getRoles().stream()
                        .map(role -> new SimpleGrantedAuthority(role.getName()))
                        .collect(Collectors.toList()));
    }

    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public ru.blog.entity.User getUserByName(String name) {
        return userRepository.getUserByName(name);
    }

    public Page<User> getPaginatedAllUsers(int pageNumber, int pageSize, String sortedBy, String order) {
        Sort sorting = Sort.by(sortedBy);
        Pageable paging = PageRequest.of(--pageNumber, pageSize, order.equals("acs") ? sorting.ascending() : sorting.descending());
        return userRepository.findAll(paging);
    }

    public void save(ru.blog.entity.User user) {
        userRepository.save(user);
    }

    public Optional<ru.blog.entity.User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}