package ru.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.blog.entity.Contact;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    List<Contact> getContactsByActive(boolean active);
}
