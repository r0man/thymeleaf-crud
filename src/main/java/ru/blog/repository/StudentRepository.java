package ru.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.blog.entity.university.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
