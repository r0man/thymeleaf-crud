package ru.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.blog.entity.university.StudentId;

public interface StudentIdRepository extends JpaRepository<StudentId, Long> {
}
