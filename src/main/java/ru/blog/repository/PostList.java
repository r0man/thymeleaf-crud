package ru.blog.repository;

import java.util.Date;

public interface PostList {

/*    Long getId();
    String getTitle();
    User getUser();
    Category getCategory();
    Long getViewedTimes();
    boolean isActive();
    Date getCreatedAt();
    Date getModifiedAt();

    interface User {
        String getName();
    }

    interface Category {
        String getName();
    }*/


    Long getId();
    String getTitle();
    User getUser();
    Category getCategory();

    interface User {
        String getName();
    }

    interface Category {
        String getName();
    }
}

