package ru.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.blog.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<CategoryIdAndName> getCategoriesByActive(@Param("active") boolean active);

    Category getCategoryByName(String name);
}
