package ru.blog.entity.university;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "student_id")
@Data
public class StudentId {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

    private Date expired;
}
